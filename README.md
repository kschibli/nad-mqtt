# NAD MQTT
Bridge that proxies the vendor specific [control protocol](https://nadelectronics.com/software/#Protocol) of NAD audio products from RS232 to MQTT. Written in Rust using
asynchronous programming.

# Building
```
cargo build --release
```

Cross compile for raspberry pi (see also [this guide](https://hackernoon.com/compiling-rust-for-the-raspberry-pi-49fdcd7df658))
```
cargo build --target=armv7-unknown-linux-gnueabihf
```

# Testing

This describes how to set up two connected virtual serial ports and a local mqtt broker (mosquitto). One simulates the serial port on the computer running `nad-mqtt` and the other simulates the serial port on the NAD product. 

## Dependencies
- docker
- socat
  ```
  sudo pacman -S socat
  ```

## Run mqtt broker
```
docker run -d --rm --name mqtt -p 1833:1833 eclipse-mosquitto
```

## Create virtual serial ports
```
sudo socat PTY,link=/dev/ttyVirtualS0,echo=0 PTY,link=/dev/ttyVirtualS1,echo=0
```

## Run the program
```
cargo run -- --host localhost:1883 --topic-prefix rust/test --serial-port ./ttyVirtualS0
```

## Open serial terminal
```
sudo socat stdio /dev/ttyVirtualS1
```

You can now see the commands that are sent to the NAD product and simulate its responses by typing them.
