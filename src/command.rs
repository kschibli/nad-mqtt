use log::error;
use mqttrs::{Packet, Publish, QosPid};
use std::convert::TryFrom;

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Command(pub String, pub char, pub Option<String>);

impl Command {
    pub(crate) fn into_packet(self, topic_prefix: String) -> Packet {
        let Command(command, _, value) = self;
        Packet::Publish(Publish {
            dup: false,
            qospid: QosPid::AtMostOnce,
            retain: true,
            payload: value.expect("Command has a value").into_bytes(),
            topic_name: [topic_prefix, command].join("/"),
        })
    }
}

impl From<String> for Command {
    fn from(s: String) -> Self {
        let mut split = s.split('=');
        let command = split
            .next()
            .ok_or_else(|| error!("Could not split {s}"))
            .expect("Could not get command")
            .into();
        let value = split
            .next()
            .ok_or_else(|| error!("Could not split {s}"))
            .expect("Could not get value")
            .into();
        Self(command, '=', Some(value))
    }
}

impl From<Command> for String {
    fn from(Command(command, operator, value): Command) -> Self {
        [command, operator.to_string(), value.unwrap_or_default()].concat()
    }
}

impl TryFrom<Packet> for Command {
    type Error = MqttCommandParseError;

    fn try_from(packet: Packet) -> Result<Self, Self::Error> {
        match packet {
            Packet::Publish(Publish {
                retain: false, // Ignore retained messages received when connecting
                mut topic_name,
                payload,
                ..
            }) => {
                let command = {
                    let command_pos = topic_name
                        .rfind('/')
                        .ok_or(MqttCommandParseError::InvalidTopic)?;
                    topic_name.drain(..=command_pos).for_each(drop);
                    topic_name
                };
                let (operator, value) = match payload.as_slice() {
                    &[op @ (b'+' | b'-' | b'?')] => (op.into(), None),
                    _ => (
                        '=',
                        Some(
                            String::from_utf8(payload)
                                .map_err(|_| MqttCommandParseError::InvalidUtf8)?,
                        ),
                    ),
                };
                Ok(Self(command, operator, value))
            }
            _ => Err(MqttCommandParseError::InvalidPacketType),
        }
    }
}

#[derive(Debug)]
pub enum MqttCommandParseError {
    InvalidUtf8,
    InvalidTopic,
    InvalidPacketType,
}
