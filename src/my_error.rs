use crate::command::MqttCommandParseError;
use std::fmt::{Display, Formatter};
use tokio_util::codec::LinesCodecError;

#[derive(Debug)]
pub enum MyError {
    MqttConnectFailure,
    MqttSubscribeFailure,
    MqttCommandParseError(MqttCommandParseError),
    Mqtt(mqttrs::Error),
    Timer(tokio::time::Error),
    Io(std::io::Error),
    LinesCodec(LinesCodecError),
}

impl From<MqttCommandParseError> for MyError {
    fn from(e: MqttCommandParseError) -> Self {
        Self::MqttCommandParseError(e)
    }
}

impl From<LinesCodecError> for MyError {
    fn from(e: LinesCodecError) -> Self {
        Self::LinesCodec(e)
    }
}

impl From<tokio::time::Error> for MyError {
    fn from(e: tokio::time::Error) -> Self {
        Self::Timer(e)
    }
}

impl From<mqttrs::Error> for MyError {
    fn from(e: mqttrs::Error) -> Self {
        Self::Mqtt(e)
    }
}

impl From<std::io::Error> for MyError {
    fn from(e: std::io::Error) -> Self {
        Self::Io(e)
    }
}

impl Display for MyError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "{:?}", self)
    }
}

impl std::error::Error for MyError {}
