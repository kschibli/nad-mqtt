use bytes::BytesMut;
use mqttrs::Packet;
use tokio_util::codec::{Decoder, Encoder};

pub struct MqttCodec;

impl MqttCodec {
    pub const fn new() -> Self {
        Self {}
    }
}

impl Decoder for MqttCodec {
    type Item = mqttrs::Packet;
    type Error = mqttrs::Error;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        mqttrs::decode(src)
    }
}

impl Encoder<Packet> for MqttCodec {
    type Error = mqttrs::Error;

    fn encode(&mut self, packet: Packet, dst: &mut BytesMut) -> Result<(), Self::Error> {
        mqttrs::encode(&packet, dst).map(drop)
    }
}
