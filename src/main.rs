#![warn(clippy::all, clippy::pedantic, clippy::nursery)]

mod command;
mod mqtt_codec;
mod my_error;

use crate::command::Command;
use crate::mqtt_codec::MqttCodec;
use crate::my_error::MyError;
use clap::{App, Arg};
use futures::{stream::select, SinkExt, StreamExt, TryStreamExt};
use log::{debug, error, info};
use mqttrs::{
    Connack, Connect, ConnectReturnCode, Packet, Pid, Protocol, QoS, Suback, Subscribe,
    SubscribeReturnCodes, SubscribeTopic,
};
use std::collections::HashSet;
use std::convert::TryFrom;
use std::env;
use std::error::Error;
use tokio::net::TcpStream;
use tokio::time::{self, Duration};
use tokio_serial::{DataBits, FlowControl, Parity, Serial, SerialPortSettings, StopBits};
use tokio_util::codec::{Framed, LinesCodec};
use uuid::Uuid;

#[derive(Debug)]
enum Item {
    ToSerial(Command),
    ToMqtt(ItemToMqtt),
}

#[derive(Debug)]
enum ItemToMqtt {
    Packet(Packet),
    Command(Command),
}

async fn connect_and_subscribe(
    rw: &mut Framed<TcpStream, MqttCodec>,
    topic_prefix: String,
    client_id: String,
    keep_alive: u16,
) -> Result<(), MyError> {
    let connect = Packet::Connect(Connect {
        protocol: Protocol::MQTT311,
        keep_alive,
        client_id,
        clean_session: true,
        last_will: None,
        username: None,
        password: None,
    });
    debug!("Sending {:?}", connect);
    rw.send(connect).await?;
    info!("Sent CONNECT");

    let alleged_connack = rw.next().await;
    debug!("Got {:?}", alleged_connack);
    match alleged_connack {
        Some(Ok(Packet::Connack(Connack {
            code: ConnectReturnCode::Accepted,
            ..
        }))) => Ok(()),
        _ => Err(MyError::MqttConnectFailure),
    }?;
    info!("Got valid CONNACK");

    let pid = Pid::new();
    let topics = vec![SubscribeTopic {
        topic_path: [topic_prefix, "+".into()].join("/"),
        qos: QoS::AtLeastOnce,
    }];
    let subscribe = Packet::Subscribe(Subscribe { pid, topics });
    debug!("Sending {:?}", subscribe);
    rw.send(subscribe).await?;
    info!("Sent SUBSCRIBE");

    let alleged_suback = rw.next().await;
    debug!("Got {:?}", alleged_suback);
    match alleged_suback {
        Some(Ok(Packet::Suback(Suback {
            pid: suback_pid,
            return_codes,
        }))) => {
            if pid != suback_pid
                || return_codes
                    .iter()
                    .any(|&c| c == SubscribeReturnCodes::Failure)
            {
                Err(MyError::MqttSubscribeFailure)
            } else {
                info!("Got valid SUBACK");
                Ok(())
            }
        }
        _ => Err(MyError::MqttSubscribeFailure),
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    // configure logging
    env::set_var(
        "RUST_LOG",
        env::var_os("RUST_LOG").unwrap_or_else(|| "debug".into()),
    );
    env_logger::init();

    let matches = App::new("nad-mqtt")
        .author("Kevin Schibli <kevin_schibli@hotmail.com>")
        .arg(
            Arg::with_name("HOST")
                .short('h')
                .long("host")
                .takes_value(true)
                .required(true)
                .about("MQTT server address (host:port)"),
        )
        .arg(
            Arg::with_name("TOPIC_PREFIX")
                .short('t')
                .long("topic-prefix")
                .takes_value(true)
                .required(true)
                .about("Channel filter to subscribe"), //FIXME
        )
        .arg(
            Arg::with_name("SERIAL_PORT")
                .short('s')
                .long("serial-port")
                .takes_value(true)
                .required(true)
                .about("Path to serial port device"),
        )
        .get_matches();

    // Process args
    let host = matches.value_of("HOST").unwrap();
    let topic_prefix = matches.value_of("TOPIC_PREFIX").unwrap().to_string();
    let serial_port = matches.value_of("SERIAL_PORT").unwrap();
    let client_id = format!("/MQTT/rust/{}", Uuid::new_v4());
    let keep_alive = 3600;

    // Prepare serial
    let serial_port = {
        let settings = SerialPortSettings {
            baud_rate: 115_200,
            data_bits: DataBits::Eight,
            flow_control: FlowControl::None,
            parity: Parity::None,
            stop_bits: StopBits::One,
            timeout: Duration::default(),
        };
        let mut serial_port =
            Serial::from_path(serial_port, &settings).expect("Failed to open serial port");
        serial_port
            .set_exclusive(false)
            .expect("Unable to set serial port exclusive");
        serial_port
    };
    let (serial_sink, serial_stream) = Framed::new(serial_port, LinesCodec::new()).split();

    // Prepare mqtt
    let tcp_stream = TcpStream::connect(host).await?;
    let mut mqtt_framed = Framed::new(tcp_stream, MqttCodec::new());
    connect_and_subscribe(
        &mut mqtt_framed,
        topic_prefix.clone(),
        client_id,
        keep_alive,
    )
    .await?;
    let (mqtt_sink, mqtt_stream) = mqtt_framed.split();

    let serial2mqtt = serial_stream
        .map_err(MyError::from)
        .inspect(|s| {
            info!("serial -> {:?}", s);
        })
        .map_ok(Command::from)
        .map_ok(ItemToMqtt::Command)
        .map_ok(Item::ToMqtt);

    let pingreq_stream = time::interval(Duration::from_secs(u64::from(keep_alive) / 2)).map(|_| {
        Ok(Packet::Pingreq)
            .map(ItemToMqtt::Packet)
            .map(Item::ToMqtt)
    });

    let mqtt2serial = mqtt_stream
        .inspect(|p| info!("mqtt   -> {:?}", p))
        .map_err(MyError::from)
        // Only keep packets that parse as command i.e. publish packets
        .try_filter_map(|packet| async { Ok(Command::try_from(packet).ok().map(Item::ToSerial)) });

    let program = select(pingreq_stream, select(mqtt2serial, serial2mqtt)).try_fold(
        (serial_sink, mqtt_sink, HashSet::<Command>::new()),
        |(mut serial_sink, mut mqtt_sink, mut pending_echos), item| async {
            match item {
                Item::ToSerial(command) => {
                    if pending_echos.remove(&command) {
                        Ok((serial_sink, mqtt_sink, pending_echos))
                    } else {
                        let line = String::from(command);
                        info!("serial <- {:?}", &line);
                        serial_sink
                            .send(line)
                            .await
                            .map_err(MyError::from)
                            .map(|_| (serial_sink, mqtt_sink, pending_echos))
                    }
                }
                Item::ToMqtt(item_to_mqtt) => {
                    let packet = match item_to_mqtt {
                        ItemToMqtt::Command(command) => {
                            pending_echos.insert(command.clone());
                            command.into_packet(topic_prefix.clone())
                        }
                        ItemToMqtt::Packet(packet) => packet,
                    };
                    info!("mqtt   <- {:?}", &packet);
                    mqtt_sink
                        .send(packet)
                        .await
                        .map_err(MyError::from)
                        .map(|_| (serial_sink, mqtt_sink, pending_echos))
                }
            }
        },
    );

    if let Err(e) = program.await {
        error!("{e}");
    };

    Ok(())
}
